# **À PROPOS DE MOI**

![](images/AVATAR.jpg)

Bonjour ou bonsoir! Je suis Paula et je suis une étudiante Erasmus à Bruxelles. Concrètement, je commence ma première année de master en architecture à l'Université Libre de Bruxelles (ULB).
Pour mieux me connaître, je viens d'Espagne et là-bas j'étudiais à l'École Technique Supérieure d’Architecture de Madrid.
Quelques curiosités sur moi. Au début, je suis un peu timide, mais quand j’ai confiance, je ne peux pas arrêter de parler. Et j'aime prendre le petit déjeuner.
Restez! Et découvrez avec moi le processus de conception d’un objet!!

## MES PROJECTS

Ce qui m’intéresse dans l’architecture ce sont les projets de communauté et de durabilité.

Je veux partager avec vous deux projects par raport a ce question.

![](images/P1.jpg)

![](images/P2.jpg)

## CHOIX DE L'OBJECT

Pour choisir l'object à travailler je voulais un artefact de mon quotidien. À ce stade, j'ai commencé a penser à ma routine et à les objects que j'utilise dans ma vie quotidienne.

Comme j'ai l'expliqué je suis une folle du petit déjeuner. Donc, mon choix c'est une tasse.

Mon but est de créer un objet esthétique et pratique. Étudier ce qui ne marche pas et l'améliorer.

![](images/TAZAS.jpg)

L’exploration continue...

![](images/TAZAS.jpg)
L’exploration continue...
