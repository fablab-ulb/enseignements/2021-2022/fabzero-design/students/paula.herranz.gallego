# **MODULE 2**: FUSION 360

On commence le processus de conception de notre objet!

Pour ameliorer un objet il faut d'abord qu'on comprende comment est-ce qu'il functionne, ses parties, ses dimensions ou sa materialité. À partir de là, on étude ses faiblesses.

Pour vous rappeler le dernier 'post' il s'agit de LA TASSE.

![](../images/taza.jpg)

Cette lecture de l'objet nous permet d'observer des questions comme la capacité (habituellement entre 300ml-340ml), les dimensions (95mm.90mm.60mm), ou les parties (corp et poignée).

Une fois qu'on a bien compris l'objet de base, la modélisation 3D de la tasse commence.

## MODÉLISATION 3D

Le Module 2 on apprendra à utiliser [FUSION 360](https://www.autodesk.com/products/fusion-360/personal) (ici pour le télécharger). 
>Fusion 360 est un logiciel de modélisation 3D de conception et de fabrication du produit.

Le but du programme est gagner du temps et de nous fournir des outils simples.

Résumé de la modélisation: on va faire un croquis de la section du corps et de la poignée, et on va les révolutionner et extruder respectivement. Et pour compliquer un peu la modélisation on va paramétrer les dimensions.
***
![](../images/1.jpg)

1. On commence par dessiner la section du corps principal. On doit suivre 'solid' > 'sketch' et sélectioner le plan dans lequel on va dessiner.

2. Là on sélectionne l'outil 'line' et on dessine la section en tenant compte des dimensions déjà étudiées.
***
![](../images/2.jpg)

3. Fusion 360 nous apporte l'outil' de paramétrage de notre objet. On doit suivre 'modify' > 'change parameters'. L'hauteur et les deux rayons seront nos paramètres.

4. Pour relier le paramètre à l'objet on doit aller à l'icône en bas à gauche > cliquer sur le bouton droit > 'edit sketch'. En cliquant deux fois sur la mesure on peut changer le numéro par le nom du paramètre. Paramétrer les dimensions nous permettra de les modifier facilement
***
![](../images/3.jpg)

5. Le temps de travailler en 3D est arrivé. On doit sélectionner l'icône de la maison qui se trouve dans le cube dans le coin supérieur à gauche.

6. On doit suivre 'solid' > et selectionner le croquis de la section. Ça nous va permettre de construir notre objet en 3D.
***
![](../images/4.jpg)

7. Pour faire le trou de la tasse on doit suivre 'solid' > 'emptying'. Et de la même façon on paramétre l'épaisseur.
***
![](../images/5.jpg)

8. Pour bien dessiner le poignée en reliant ses dimensions  au corps, la manière la plus facile est de découper l'objet. On doit suivre 'tools' > 'secton analysis' et selectionner le plan de coupe.

9. Avec l'aide de l'outil 'sketch' on dessine le corps suivant (le poignée) et on le joint au corps principal. On s'assure que les deux parties sont coupées.
***
![](../images/6.jpg)

10. Une fois les deux parties sont coupées, la section se défait et on obtient la tasse complète.

11. Les details sont également importants. Si on suivre 'solid' > 'splice', les boucliers des corps sont résolus.
***
![](../images/7.jpg)

12. Pour finaliser, on peut paramétrer plus de dimensions: les épeisseurs du poignée ou les épissure du corps.

# RÉSULTAT!!

![](../images/8.jpg)

## 3D
  * <a href="https://a360.co/3FalJgC" target="_blank" style="font-weight: bold; color: #1CAAD9;">TASSE EN 3D</a>

## LIENS UTILES

* [INSTALLATION FUSION 360](https://www.autodesk.com/products/fusion-360/personal)
* [GUIDE FUSION 360](https://formlabs.com/blog/fusion-360-tutorial-basics-and-tips-for-3d-printing/)
