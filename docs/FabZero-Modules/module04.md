#  **MODULE 4**: DÉCOUPE ASSISTÉE PAR ORDINATEUR

La formation du module 4 consiste à apprendre le fonctionnement et des paramètres de la **DÉCOUPE LASER** [Guide](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/LaserCutters.md)L'exercise à faire cette semaine est concevir une lamp à partir d'une source d'éclairage et toujours inspirée par l'obejet, dans notre cas, la tasse. Il y a une seule règle, il faut utiliser une matériau, une feuille de Polypropyène de taille 50cm x 80cm. Ainsi on fera une experimentation pour nous familiariser aves les machines.

Pour commencer la partie amusante, on doit connaître (même processus que l'impression 3D) quelques normes, comprennent les machines du FabLab, précautions et les materiaux.

### LES MACHINES DU FABLAB.

![](../images/41.jpg)

1. Les premieres images sont de la machines **LASERSAUR** [Guide](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Lasersaur.md) C'est un découpeur laser open source. La curiosité de cette machine est qu'elle est construite par l'utilisateur, FabLab dans ce cas. Elle nous permet effectuer découpes de grande taille étant donné que la surface est 122cm x 61cm et une hauteur maximal de 12cm.
La vitesse maximal est de 6000 mm/min et la puissance de 100 W. Le logiciel de contrôle de la machine est **Driveboard App**, et les formats de fichier compatibles sont: DXF, version R14 si possible (à exporter de Rhinoceros); SVG, (à exporter de Inkscape); ou DBA, que permet configurer puissance et vitesse.

2. La deuxième image est de la machine **EPILOG FUSION PRO 32** [Guide](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/EpilogFusion.md),  elle est l’une des machines les plus rapides du marché. La surface de découpe est 81cm x 51 et l'hauteur maximal est 31cm. Sa puissance est 60W. Pour envoier notre travail à la machine il faut dessiner sur Inkscape et exporter le fichier sur le format SVG.

Pour avoir une bonne découpe de la pièce on doit tenir compte deux facteurs. D'un côté la pièce doit être dessiné en millimètres et d'otre côté à l'heure de le exporter on réduit l'épaisseur des traits à 0,01 mm. De cette manière notre découpe sera la plus fine possible.

### LES MÁTERIAUX

![](../images/42.jpg)

1. Les matériaux **recommeandés** sont: bois contreplaqué, acrylique, papier, carton et textiles.

2. Il y a aussi des matériaux **déconseillés** car le fumée peut être épaisse et nocive. Ce sont: le tableau de bord du bois de la fibre des médias denses (MDF), acrylonitrile Butadiène Styrène (ABS), le polystyrène (PS), le polyéthylène (PE),le polyéthylène téréphthalate (PET), le polypropylène
Composites à base de fibres (PP) et les métaux (impossible à découper)

3. La dernier image est des matériaux **interdits** à cause de sa toxicité. Ce sont:
le PVC, le cuivre, le téflon (PTFE), la résine phénolique ou époxy, le vinyl, le simili-cuir, le cuir animal (mal odeur)

### PRÉCAUTIONS

![](../images/43.jpg)


On doit  être conscient qu’il y a un risque d’incendie lors de l’utilisation des machines. Alors, pour éviter tout risque d'incendie il y a six règles:

1. Connaître les matériaux recommandés, déconseillés et interdits et comme est-ce qu'ils fonctionnement dans les machines.

2. Cette image correspond à la machine Lasersaur. Il faut activer l'air comprimé en ouvrant la vanne.

3. Cet bouton vert correspond à la machine Lasersaur. Il sert à allumer l'extracteur de fumée.

4. Cet bouton noir correspond à la machine Lasersaur. Il sert à allumer le refroidisseur à eau.

5. Savoir où trouver un extincteur au CO2

6. Savoir où est le bouton d’arrêt d’urgence. L'image ci-dessus correspond à la machine Lasersaur, si on appuie, elle s'arrêt, mais si on le tourne, elle s'allume. L'image ci-dessous correspond à la machine Epilog Fusion PRO 32.

7. Et finallement, toujours rester à proximité de la machine jusqu'à la fin de la découpe.

Ces deux autres précautions sont très importantes aussi: ne pas regarder le laser pendant qu’il fonctionne et lorsque lq découpe est terminée attendre un peu pour ouvrir la machine.

## PREMIERS TESTS

Une fois on a étudié la partie technique des machines, il faut faire quelques premiers tests. Les professeurs nous ont demandé de nous mettre en groupe de cinq personnes et de faire ce qui suit:

Le material avec lequel on travaille, dans cet exercise de conception de une lampe, est le polypropylène. On doit savoir que chaque material a un comportement different sur les autres. Le but de cet premier exercise consiste à connaître le comportement de notre material.

Les machines de decoupe laser funtionnent avec deux paramètres: la **puissance** et la **vitesse**. Selon la valeur qu’on attribue on obtiendra une **gravure** (une marque sur le material) ou un **decoupe**.
***
![](../images/module45.jpg)

1. D’abord on a dessiné 36 carrés, chacun avec une couleur différente, afin d’attribuer une valeur de puissance (nombres à la vertical) et une valeur de vitesse (nombres à l'horizontale) à chacun. De cette façon, on saura comment le matériau, le polypropylène, fonctionne.

 Dans cet cas là on a dessiné sur le logiciel [INKSCAPE](https://inkscape.org/release/inkscape-1.1.1/) qui nous permet d'exporter le fichier directement sur le format SVG, compatible avec les machines.

 On a utilisé la machine *LASERSAUR* qu'utilise le logiciel de contrôle "Driveboard App". Alors on a importé le fichier est on a changé les valeurs de vitesse (F) et de puissance (%) de chaque carré.

 Avec le résultat final, on a réalisé que le polypropylène est un matériau très sensible à la chaleur. Il a besoin de faibles valeurs pour pouvoir faire la gravure.

 >PETIT RAPPEL!! Allumer la machine avec le bouton rouge, allumer les deux extracteurs de fumée, allumer le refroidissement de l’eau et ouvrir la valve d’alimentation d’air comprimé. Enfin, quand le decoupe laser est fini, éteignez-le avant de l’ouvrir.


## EXPERIMENTATION

![](../images/module41.jpg)

1. Au cours, nous ont demandé de décrire notre objet en quelques mots. Pour moi, la tasse est quotidienne, rigide. Cependant, pour moi, une lampe doit être tout le contraire : ne pas quotidienne, évocateur, qui est compris avec l’espace, qui coule...

 Sans le dessin je suis incapable de concevir un objet. l’idée est de garder l’essence d’une tasse mais d’obtenir ce que nous suggère une lampe. Ces premiers croquis et maquettes ne me convainquent pas, mais ils sont ce qui va nous mener à un résultat final. Le processus est très important.
***
![](../images/module42.jpg)

2. A partir de ici, l'experimentation commence a avoir une sens. On commence à explorer par la section de l'object. Dans cet premier testation la section est vertical.

 La vérité est que les ombres que la lampe génère me plaisent, et il me semble que je garde l’essence de ce que je cherchais. Cependant, je sens que c’est peu.
***
![](../images/module43.jpg)

3. Dans ce deuxième testation la section est horizontal, ça veut dire, le cercle. Nous atteignons notre objectif..
***
![](../images/module44.jpg)

4. Une outil fundamental pour moi est la modèlisation 3D de l'objet. Dans mon cas, je connais très bien le logiciel Rhinoceros 3D. Il est destiné à la conception, à modèler, à présenter, à analyser, à réaliser, etc.

 J’ai commencé à jouer avec des cercles jusqu’à ce que j’obtienne une forme que j’aimais. A partir de là, je trouve des façons de construire la lampe.

## IL EST TEMPS DE COUPER!!

Comme lors des premiers tests, j’ai utilisé la machine Lasersaur, cette fois, j’utiliserai la machine **EPILOG FUSION PRO 32**. De cette façon, je peux comprendre comment les deux fonctionnent.
***
![](../images/module46.jpg)

Une fois qu’on a l’objet modelé, on doit le dessiner en 2D, qui est la dimension que la découpeuse laser supporte.

1. Il y a une commande dans Rhinoceros 3D avec le nom de "unroll developable surface" qui nous permet de déployer un objet. Dans ce cas, on sélectionne pièce par pièce de notre objet dans le but de les avoir toutes en 2D.

2. Ces pièces doivent être transformées en dessin, c’est-à-dire passées en ligne. Avec la commando "Make 2-D drawing" on peut l’obtenir.

3. Le problème est que depuis ce logiciel, on ne peut pas exporter le fichier dans le format SVG, compatible avec la machine. Pour obtenir ce format, on doit travailler avec des programmes comme Inkscape ou Illustrator. Elle utilisera personnellement Illustrator, pour cela il faudra exporter les dessins en PDF et les importer dans le logiciel.

Pour notre objet, on n’a besoin que de couper, donc on peut dessiner les lignes avec une seule couleur.

On ne peut pas oublier d’exporter le fichier en mm!!
***
![](../images/module47.jpg)

4. La première étape consiste à ouvrir le document au format SVG qu'on a précédemment exporté depuis Illustrator. L’ordinateur de la découpeuse laser fonctionne avec Inkscape. Ici on vérifie que le document est en mm et a la taille qu’elle veut. S’il y a des erreurs, on peut facilement les changer.

5. Après avoir corrigé ces paramètres, on envoie le document à imprimer. C’est le moment où on décide la vitesse et la puissance avec laquelle nous voulons couper nos pièces. On doit sélectionner **autofocus** dans "palpeur", le **type d’opération** en "découpe", et la **fréquence** toujours à 100%.

 Comme on a déjà fait un test pour voir comment se comporte le polypropylène, j’ai décidé de mettre la vitesse à 40% et la puissance à 80%.

6. On a finalement lancé la pièce à couper en cliquant sur "imprimer".

 Ne pas oublier d’allumer et d’éteindre (avant d’ouvrir) l’extracteur de fumées (bouton vert). Celui-ci est situé à droite de l’ordinateur.

![](../images/module48.jpg)

Si dans un autre cas nos pièces avaient besoin d’être découpées et gravées, il faudrait changer la couleur des lignes. Dans cet exemple, on peut voir qu’il y a trois couleurs différentes. Ceux-ci peuvent être édités à la dernière étape, en cliquant sur "+ Couleur" et en établissant différentes vitesses et puissances. On peut également choisir l’ordre de coupe : par exemple, couper d’abord la couleur verte, puis le bleu et enfin le noir.

## RÉSULTATS!!!

![](../images/collage.jpg)

Au début, on pose la lampe pour que la lumière vienne d’en haut. Pour cela, on pose un cercle intermédiaire qui servira à tenir l’ampoule.

Cette approche ne fonctionne pas. Je remarque que ce cercle intermédiaire déforme le reste des pièces.

![](../images/finallampara.jpg)

En plaçant une source d’éclairage, dans mon cas une ampoule, je me rends compte que le système ne fonctionne pas : l’ampoule est tordue. À son tour, l’éclairage et les ombres que la lampe génère ne sont pas ce que je cherchais.

![](../images/module49.jpg)

Il me vient à l’esprit d’éclairer la lampe par le bas, de cette façon les ombres qu’elle génère sont plus suggestives et la lumière qu’on obtient est indirecte.

## LIENS UTILES

* [GUIDE DÉCOUPE LASER](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/LaserCutters.md)
* [GUIDE LASERSAUR](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/Lasersaur.md)
* [GUIDE EPILOG FUSION PRO 32](https://gitlab.com/fablab-ulb/enseignements/2020-2021/fabzero/machines/-/blob/master/EpilogFusion.md)
