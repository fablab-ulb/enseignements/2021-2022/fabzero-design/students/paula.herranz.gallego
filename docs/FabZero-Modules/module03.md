# **MODULE 3**: IMPRESSION 3D

'L’impression 3D est un ensemble de processus qui produisent des objets par addition de matériaux en couches correspondant aux sections transversales successives d’un modèle 3D'.

Lors de ce premier contact avec les machines d’impression 3D, de petits tests de l’objet avec lequel on travaille seront imprimés.

Connaître le funcionnement et configuration des machines d'impression 3D est nécesaire pour une bonne utilisation.

À cette fin, la première étape consiste à installer de [PRUSA SLICER](https://www.prusa3d.com/prusaslicer/) (ici pour le télécharger), un logiciel gratuit pour configurer l'impresion 3D de notre objet.

## PARAMÈTRE

![](../images/9.jpg)

1. Dans le logiciel **FUSION 360**, après moduler l'object à imprimir, il faut l'exporter au format **STL**, celui correspondant au format supporte pour 'PRUSA SLICER'.

2. Dans le logiciel **PRUSA SLICER** on importe le fichier sur le format **STL**. Voilà, notre objet est dans le plateau prêt a configurer.
***
![](../images/32.jpg)

3. Les premiers paramètres sont effectués en cliquant sur **configuration d'impresion**. L'hauteur de la couche doit toujours être 0,2 (mm ou mm%) et sur les parois verticales, les périmètres doivent être 3.

4. Les coques horizontales, la qualité et avancé peuvent être laisser par default sauf si nécessaire.
***
![](../images/33.jpg)

5. Après cela, le remplissage nous permet d’être créatifs et de choisir le motif de remplissage de l’objet qu’on désire. J’ai ajouté une image des différents motifs que le logiciel nous offre. En même temps, la densité de remplissage doit comprendre une valeur d’environ 15%.
***
![](../images/34.jpg)

6. La jupe et le bordure servent à définir l’objet dans le plateau pendant l’impression. Donc, puisque notre impression sera d’un petit objet, les valeurs sont conservées par défaut.

7. Les options suivantes nous permettent d’ajouter un support d’impression si notre objet en a besoin.
***
![](../images/35.jpg)

8. Il est très nécessaire de connaître le matériau (le rouleau de couleur et la machine) avec lequel on travaille. Sur l’image du rouleau de couleur, on observe la température avec laquelle l’impression est configurée. Dans notre cas, la température est de 215ºC. On doit suivre **Configuration du filament** > **Filaments** > **température**.

9. À droite de l’écran on trouve une option pour sélectionner le type de machine d’impression 3D. **ORIGINAL PRUSA i3 MK3**, est la machine qu'on a dans FabLab. Pour vérifier, le plateau de notre ordinateur doit être le même que celui des machines.
***
![](../images/36.jpg)

10. Avant de laminer l'objet il y a d'autres paramètres possibles. Il faut tenir compte que la taille du plateau n’est pas très grande. Pour ce faire, on devra utiliser le facteur d’échelle pour ajuster la taille de l'objet.

11. On est prêt pour laminer et exporter le code que va nous servir à imprimer l'objet. Il y a aussi une fenêtre qui affiche le temps d'impresion.

## PROCESSUS... ET RESULTATS!!

![](../images/39.jpg)

Mon premier contact avec les machines d’impression 3D a été très excitant. J’ai vraiment aimé voir comment petit à petit l’objet se construisait.

Heureusement, je n’ai eu qu’un seul test, parce qu’il a été bien imprimé à la première. Cependant, je ne vais pas vous mentir, j’ai eu peur que le poignée se cassait.

![](../images/module31.jpg)

1. Pour résoudre la poignée, on aurait pu utiliser du matériel de support pour l’impression en cliquant sur "générer du matériel de support". Mais on a sélectionné "supports générés automatiquement", recommandé par le logiciel. De cette façon, l’angle de la poignée peut résister sans se casser et elle n’a pas besoin de matériel de support.

2. Bien que la durée d’impression soit de deux heures, j’ai augmenté la vitesse pendant l’impression. De là, des défauts ont été créés dans la pièce.

![](../images/37.jpg)

Voici les resultats finaux!

## LIENS UTILES

* [INSTALLATION PRUSA SLICER](https://www.prusa3d.com/prusaslicer/)
* [PRUSA SLICER GUIDE](https://help.prusa3d.com/en/category/prusaslicer_204)


Voici les resultats finaux!!
