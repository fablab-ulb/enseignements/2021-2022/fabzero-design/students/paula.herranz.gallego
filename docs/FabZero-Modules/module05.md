# **MODULE 5**: SHAPER

Dans ce dernier module, l’objectif est d’apprendre le fonctionnement de la machine Shaper[Guide](https://assets.shapertools.com/manual/Shaper_Origin_Product_Manual.pdf)

>La machine Shaper est un type de machine-outil qui utilise le mouvement relatif entre la pièce et l’outil de coupe

![](../images/module53.jpg)

Voici un petit schéma des parties de la machine. On parlera de chacune d’entre elles tout au long de cette publication. On verra à quoi ils servent et quand ils doivent être utilisés.

## RÈGLES ET PRÉCAUTIONS

Avant de commencer le mis en marche, il est nécessaire de connaître les règles et les précautions à faire un bon usage.

Ici quelques spécification sur le machine:

1. La profondeur maximum à découper est 43 mm.

2. Le diamètre de collet est 8 mm.

3. Le fichier doit être dans le format supporté: SVG.

### PRÉCAUTIONS

![](../images/module52.jpg)

1. La surface sur laquelle on travaille doit être stable et plate. Cela peut être une table ou une planche en bois, entre autres idées.

2. Le matériau à travailler doit être bien fixé à la surface. On doit trouver un moyen de garder le matériel bien attaché et ne pas bouger pendant que la coupe.

3. Le bouton rouge est pour relever la fraiseuse. Il faut toujours la relever avant de passer d'une forme à une autre. Et la même chose avec le bouton vert, on doit appuyer sur au moment de commencer à baisser la fraiseuse.

4. Utiliser un aspirateur de poussières. Ça c'est le aspirateur qu'on a dans FabLab.

5. Afin de achievre un meilleur contrôle sur la machine, les pieds et le corps doivent mantenir une position solide et équilibre.

6. Si on porte des vêtements larges, des bijoux ou des cheveux détachés peut être dangereux, elles risquent de brûler avec des pièces mobiles.

## MISE EN MARCHE

Comme on l’a expliqué, pour mettre en service la coupe la première chose qu’on doit faire est de trouver une surface plate et stable. Et, en plus, assurons-nous que le matériel va bien fixer et ne pas bouger.

![](../images/module51.jpg)

1. Sur notre surface préalablement choisie il est nécessaire de délimiter une aire, notre aire de travail. Celui-ci est délimité par des bandes afin que la machine reconnaisse que la surface est plate. Il n’est pas nécessaire que ces bandes soient parallèles, elles doivent simplement bien délimiter notre périmètre et être placées dans le même plan.

2. Dans cette deuxième étape, on apprendra à utiliser l’appareil photo et l’écran tactile. Une fois qu'on a' notre zone de travail délimité, la machine doit le reconnaître. Pour ce faire, on appuie sur "new scan" et avec la caméra elle pointe et attend jusqu’à ce que toutes les bandes soient reconnues. Une fois que cela se produit, elle appuie sur "finish"

![](../images/module54.jpg)

3. Il est temps d’importer notre fichier dans le format compatible : SVG. Pour ce faire, on doit mettre l’USB qui contient le fichier dans le port.

4. Une fois qu’on a mis le dessin au-dessus du secteur où on va couper, on appuie sur "place".

 Après cela avec l’option "fraiser", en appuyant sur "cut", on peut effectuer quelques ajustements par rapport au type de coupe. Après avoir fait notre choix, on appuie sur "start Cutting", pour commencer à couper.

 On ne peut pas oublier d’appuyer sur le bouton vert, situé à droite, pour abaisser la fraiseuse.

![](../images/module55.jpg)

5. Dans ce paramètre, on peut changer le type de ligne par rapport à la trace.

6. Dans ce réglage, on peut changer la profondeur que nous voulons que la pièce à découper ait.

Les dernières images qu’on peut avoir comme référence au moment de changer les paramètres. La première nous montre les différents types de ligne. La seconde (dans laquelle il est écrit 1, 3, 5, 6) nous montre les différentes vitesses qu’on peut choisir. La troisième les différents offset. Et enfin, les passes qu’on peut faire.

## LIENS UTILES 

* [GUIDE SHAPER](https://assets.shapertools.com/manual/Shaper_Origin_Product_Manual.pdf)

* [VIDEO TUTORIEL](https://www.youtube.com/watch?v=sdsMOPZFMRQ)
