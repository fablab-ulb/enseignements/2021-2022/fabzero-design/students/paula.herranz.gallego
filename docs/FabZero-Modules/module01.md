# **MODULE 1**: INSTALLATION GIT

Pendant cinc semaines le travail à faire est bien se documenter et experimenter, à travers des differentes exercises, le functionement des machines de FabLab. Cela est nécessaire pour des raisons de sécurité et d'assurance. Alors, on appredera logiciels pour **MODÉLISATION 3D**, comme Fusion 360, **IMPRESION 3D** et **DÉCOUPE LASER**.

Dans ce premier module le but est d'apprendre sur le site **GITLAB**. Ce sera le blog où vous pourrez m’accompagner dans ce premier processus d’apprentissage et prochainement dans un processus de conception d’un objet.
***
![](../images/module11.jpg)

1. **GIT** est le logiciel qu'on doit [INSTALLER](https://git-scm.com/?fbclid=IwAR2VWCftFIG-EXkaMXU64S0GRk0px3sGUP0iUV4nyzS289gnNeK-xDGqQz8) sur notre ordinateur à fin de utiliser GitLab.
> Git est un système de contrôle de version distribué libre et open source conçu pour gérer tout, des petits aux très grands projets avec rapidité et efficacité.

 En cliquant sur "downloads", le site nous permet le télécharger si notre système est: macOS, Windows ou Linux.

2. Git a besoin de **Shell Bash** pour être configurer. Dans notre cas
(Windows 10) il se trouve par défault, mais il faut l'activer. Une fois on a fini tous les étapes, on ouvre la terminal et il doit apparaître sur l’écran un petit onglet noir. Après ça, on écrit *git --version* à fin de bien terminer l'installation. Désormais, on peut ouvrir la terminal avec le nom de **Git Bush**.

3. On commence à [DÉFINIR](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html?fbclid=IwAR2pG7_gmC4MOJ84CCz7-YJfmYwO2_Dft2KDXGgPNQoa2dx_88O4Y_pekvs#install-git) le logiciel en nous identifiant avec le même email et le même nom de GitLab. On doit écrire encore *git config --global user.name "your_username"* et remplacer le pseudo (on peut le trouver dans nôtre profil de GitLab) et *git config --global user.email "your_email_address@examplecom"* et remplacer l'email par le notre. Finallement avec *git config --global --list*, on vérifie notre identification.


## CONFIGURATION

![](../images/module12.jpg)

Après ces prèmieres paramètres, on doit savoir que GitLab functionne avec un **repository**, où tous les fichiers de GitLab sont enregistrés. Pour "connecter" le "repository" à notre ordinateur, il faut le cloner.
Cloner sert à pouvoir modifier le contenu des fichier de notre ordinateur sans avoir à entrer dans GitLab.

1. Le prémiere étape pour cloner un "repository" sur notre ordinateur est **"Creating a fork"**. Ceci sert à créer une copie du projet dans notre espace personnel, notre espace GitLab.

2. Deuxièmement, pour connecter GitLab et Git de manière secure, on doit créer un **clé SSH**. Il y a deux options à choisir, ED25519 et RSA. Personnallement, j'ai choisi ED25519 pour être plus sûr et efficace.

 Donc, une fois qu’on a choisi le mot de passe qui nous semble le plus approprié, on écrit *ssh-keygen -t ed25519 -C "comment"* (pour ED25519) ou *ssh-keygen -t rsa -b 2048 -C "<comment>"et on remplace "comment"* (pour RSA) et on remplace "comment" par "clé". Ce sera le nom de notre mot de passe de sécurité, qui est à l’intérieur du rectangle rose.

3. En avant notre mot de passe localisée, il faut la copie dans notre site de GitLab pour l'enregirtrer. Pour ça on suivre  *edit profile* > *SSH KEYS*.

 Pour verifier notre identification, on retourne à la terminal GitBash et on écrit *ssh -T git@ gitlab.example.com*. Et voilà on est prêt à cloner le "repository".
***
![](../images/module13.jpg)

4. [ATOM](https://atom.io/) (ici pour le télecharger) est le logiciel dans lequel on va écrire tout ça pour l'envoyer à GitLab.
>Atom est un éditeur de texte libre pour macOS, GNU/Linux et Windows développé par GitHub.

5. La dernière étape enfin! On crée un dossier sur notre ordinateur pour pouvoir cloner le projet. On ouvre ce dossier avec le terminal Git Bash, dans lequel on copiera *git clone git@ gitlab.com:fablab-ulb/enseignements/2021-2022/fabzero-design/students/paula.herranz.gallego.git*. On peut le trouver en cliquant sur l’onglet "cloner" dans notre espace personnel de GitLab.

Maintenant, nous avons vraiment notre projet prêt pour commencer à le documenter sur Atom.

## CRÉATION DU CONTENU

**Atom** sera le logiciel qui nous acompagnara pendant le cours afin de créer le contenu de nos expérimentations. Git [Guide](https://docs.github.com/es/github/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax) utilise des codes pour saisir du texte, des images ou des vidéos. Il est intéressant de savoir cela pour bien documenter les publications et les rendre plus dianamiques.

### TEXTE

![](../images/module14.jpg)

1. Comme j'ai dèjá expliqué, personallement j'utilisarai Atom, mais si c'est plus comode pour vous, vous avez la possibilité de documenter votre processus directement sur GitLab. Pour ce faire on doit aller dans notre espace personnel GitLab et sélectionner le projet avec lequel on veut travailler. Ici en cliquant sur "edit" on est prêt à écrire nos publications. GitLab utilise le même code texte.

2. C'est vrai que GitLab peut être plus facile, mais personnallement je trouve deux avantages intéressants dans Atom. D'un part, on n'a pas besoin d'internet pour l'utiliser. Et d'autre côté, il y a une option pour prévisualiser le travail et voir que tout fonctionne correctement.

Dans cette section, je vais écrire les **CODES D'INTRODUCTION DE TEXTE** que j’utilise:

* Si on met # au début ce texte est un titre.
* Si on met ## au début ce texte es un titre plus petit.
* Si on met ** au début et à la fin **ce texte est en gras**.
* Si on met * au début et à la fin *ce texte est en italique*.
* Si on met ** au début et à la fin ***ce texte est en gras et on italique***.
* Si on met > au début  > ce texte est une citation.
* Si on met *** une ligne est créée qu’on peut utiliser pour souligner ou séparer des sections.
* Si on met crochets [texte] + parenthèses (link) [Ce texte est un link](link)


### IMAGES

![](../images/module15.jpg)

1. Le projet crée dans le dossier de notre ordinateur contiene un section d'images. On y enregistre toutes les images qu'on veut publier sur GitLab. Encore une fois, on va dans notre espace personnel et on sélectionne le dossier "images" et puis "upload file" pour les télécharger sur le site GitLab.

2. Ça c'est le code de text pour ajouter les images.

On doit tenir compte la taille des images. Si la taille de nos documents est trop grande, notre plateforme pourrait s’effondrer. Pour que cela n’arrive pas, on doit utiliser des logiciels spécifiques pour compresser nos images.

## PUSH SUR GITLAB

![](../images/module16.jpg)

1. Une fois qu’on a notre documentation prêt à envoyer à GitLab, on clique "stage" sur tout ce que nous voulons qu’il fasse partie de la prochaine confirmation. I y a l'option **"stage all"**.

 Les fichiers qui ont des conflits de fusion apparaîtront dans la liste **"merge conflits"**. Si on clique sur le fichier on peut l'ouvritr. Là, on peut résoudre le conflit. Une fois terminé, on doit "stage" le fichier et le valider.

2. Une fois que on a mis en place nos modifications, on entre un message de validation. En cliquant sur le bouton **"Commit to main"** on valide l’action.

3. Finallement, Lorsque on est prêt à partager nos modifications avec les autres, on clique sur le bouton **"Push"** dans la barre d’état.

 Les options **"Fetch"** et **"Pull"** ne servent que si nous partageons notre projet avec plus de personnes. Mais dans ce cas, c’est individuel. "Fetch" sert à voir si un autre membre a fait des changements, et "pull" pour fusionner les changements sur notre propre ordinateur.

## LIENS UTILES

* [INSTALLATION GIT](https://git-scm.com/?fbclid=IwAR2VWCftFIG-EXkaMXU64S0GRk0px3sGUP0iUV4nyzS289gnNeK-xDGqQz8)
* [GIT GUIDE](https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html?fbclid=IwAR2pG7_gmC4MOJ84CCz7-YJfmYwO2_Dft2KDXGgPNQoa2dx_88O4Y_pekvs#install-git)
* [INSTALLATION ATOM](https://atom.io/)
* [SYNTAXE D'ÉCRITURE GIT](https://docs.github.com/es/github/writing-on-github/getting-started-with-writing-and-formatting-on-github/basic-writing-and-formatting-syntax)
* [ATOM GUIDE](https://flight-manual.atom.io/using-atom/sections/github-package/)
