# **LA CHALEUR DU DESIGN** PROJET FINAL

Le temps de concevir l'objet final a arrivé. Étant donné qu'il est la premier fois qu'on fait ce processus, le professeur nous a possé  des question pour reflexir et à tenir compte à l'heure de la conception.

Il y a quelque manière de clasifier les objets? Si on fait un exercise d'observation et de reflexion sur les objet, on nous rends compte qu'il y a deux types: les objets outils et les objets signe, qui sert à rien. Mais le but de notre project final est de concevir un **objet outil.**

Pour atteindre cet objectif, au cours nous ont enseigné une possible procédure à mener à bien:

1. **Déconstruction de la fonction de l'objet.** Comprendre l’objet dans sa totalité en étant observateur, curieux, méthodique et précis. Se poser des questions dans le but de comprendre leur utilisation et leurs problèmes. Il s’agit d’avoir une vision de l’objet différente de celle qu’on avait auparavant.

2. **Résoudre un vrai problème.** Dans cette deuxième étape, on analyse et étudie les limites que notre objet a. On doit être vraiment critique et trouver des faiblesses, aussi minimes soient-elles. Une fois qu’on aura compris cela, on pourra proposer de nouvelles solutions.

3. **Comprendre les dimensions spécifiques.** Mesurer l’objet pour comprendre s’il est capable de s’adapter au corps qui l’utilise. Si celui-ci est capable de s’adapter aux différentes actions que le corps humain accomplit avec lui.

4. **Esquisse, essai-erreurs, améloriations.** Plus on fait de tests, plus on a de chances d’atteindre notre objectif. Exploration de différentes techniques et conceptions avec les machines qui sont à notre disposition à Fablab. Partager nos résultats avec nos collègues et professeurs nous aidera également à continuer à nous améliorer.

5. **Adaptation, difussion édition.** L’objet doit être unique et pouvoir s’adapter à tout le monde. Il se réduit à son expression maximale.

6. **Un nom, un pictograme.** Trouver un bon nom pour notre objet permet de l’inclure dans un contexte et dans un endroit.
***
![](./images/tazacasa.jpg)

Un petit rappel. L’objet que j’ai choisi est une **tasse**. En particulier cette tasse que j’ai à la maison.

> Une tasse est un petit récipient de forme ovide, cylindrique ou demi-sphérique, généralement muni d'une anse, et qui sert à boire.

Pour moi, le petit déjeuner est indispensable, donc cet objet de ma vie quotidienne est un défi.

Trouver un équilibre : se réchauffer les mains sans se brûler. C’est ça le projet à développer. Une tasse : qui recueille vos mains, vous fait profiter du moment intime et quotidien de boire une boisson chaude. 


## 1. DÉCONSTRUCTION DE LA FONCTION DE L'OBJET

Nous rencontrons nos collègues pour analyser l’objet. Plus il y a de points de vue sur un même objet, plus l’analyse est riche.

Le matériau dans lequel la tasse est fabriquée est la céramique. Plus tard, elle a été peinte en vert puis émaillée.

Sur la forme de l’objet on peut dire que la pièce principale est en forme de cône tronqué. La base qui soutient l’objet est un cercle. Celui-ci se répète tout au long de la pièce étant de plus en plus grand.

La pièce secondaire, la poignée, est en forme de demi-cercle. Celle-ci essaie de créer une façon plus simple de prendre la pièce principale.

![](./images/esquema.jpg)

Pointer et dessiner tout dans mon cahier m’est toujours utile au travail. A travers le dessin, je me souviens de ce dont nous avons parlé et je découvre de nouvelles idées.

Les questions qui nous préoccupent le plus sont le matériel, la poignée et la forme

## 2. RÉSOUDRE UN VRAI PROBLÈME

![](./images/peterytaza.jpg)

Peter Opsvik est un designer industriel norvégien qui consacre son travail à la conception de chaises innovantes et ergonomiques. La première image est un exercice que le concepteur fait. Il consiste à utiliser un objet, le cas échéant la chaise, de différentes façons. L’objectif est de découvrir les limites de l’objet. Le concepteur disait qu’il s’agit de trouver des faiblesses dans des solutions déjà données.

Cette expérience m’a paru tellement intéressante et utile que j’ai voulu la recréer avec mon objet, la tasse. Pour faire les photos et j’ai basé sur différentes façons de saisir la tasse. À son tour, j’ai utilisé les éléments avec lesquels ma tasse interagit : ma main, une grande cuillère et du lait.

Grâce à l’exercice, j’ai pu constater que la poignée n’est pas pratique et j’ai ajouté deux nouveaux problèmes. D’une part, la cuillère (en métal) produit un son très désagréable en touchant la tasse (en céramique). Et d’un autre côté, si je veux changer le lait de la tasse à un autre bol, ce n’est pas pratique, car il tombe n’importe où sauf dans le bol que je veux.

Enfin, je peux dire que les trois questions à aborder dans le projet sont la matérialité, la poignée et la forme.

En étudiant **la matière,** je me rends compte que je rencontre trois problèmes. Fabriqué en céramique, je souligne, il génère un son désagréable quand il est touché par la cuillère. Deuxièmement, la céramique laisse passer la chaleur, donc si je veux mettre une boisson chaude à l’intérieur, l’extérieur est très chaud. Et enfin, je trouve difficile de le fabriquer avec les machines de FabLab.

Est-ce que **la poignée** est nécessaire ? D’une part je trouve sa taille appropriée pour moi, mais je pense qu’elle est petite et ne pourrait pas s’adapter à tous les corps. Et d’un autre côté, si la poignée existe, il est difficile d’empiler quelques tasses dans les autres pour les garder dans le placard et prennent beaucoup de place.

La dernière question est **la forme.** Celle-ci est cohérente avec les problèmes mentionnés ci-dessus. Si une solution leur était donnée, la forme devrait changer.

## 3. COMPRENDRE LES DIMENSIONS SPÉCIFIQUES

![](./images/medidas.png)

Pour comprendre les mesures d’un objet, il faut aussi comprendre celles des éléments qui interagissent avec lui. J’ai donc analysé les mesures de ma tasse, les mesures de la cuillère que j’utilise et les mesures de ma main.

## 4. ESQUISSE, ESSAI-ERREURS, AMÉLORIATIONS

### FORME

![](./images/prueba1.jpg)

Après les trois premières étapes d’analyse des problèmes et de compréhension du fonctionnement de l’objet, il est temps de faire des essais et des essais jusqu’à l’objet final.

Le premier test que j’ai fait est sur la forme. Ma façon de travailler est très graphique, donc j’ai pris l’objet et j’ai noté les premiers changements à faire.

Ce que j’aime le plus c’est boire une tasse de lait chaud le matin et que la tasse réchauffe mes mains. Donc j’ai réalisé que l’objet doit être plus confortable et parfait pour la taille de mes mains.

Annotations : la poignée est très petite / si je tiens la tasse comme dans la troisième image la taille est petite.

*TEST 1*

![](./images/esq1.jpg)

Dans ces schémas je vais noter dans une couleur bleue les changements et dans une couleur rose les aspects qui restent.

Dans ce premier test j’essaie de trouver une autre solution qui remplisse la même fonction de la poignée mais d’une manière plus confortable. Ce que j’ai conçu est une section adaptée aux doigts.

D’autre part, la coupe est réduite en hauteur et la largeur du rayon supérieur est augmentée.

![](./images/prueba2.jpg)

Ce test ne fonctionne pas du tout. Les deux décisions prises n’améliorent pas l’objet, mais l’aggravent. De cette façon, il est plus inconfortable.

Dans le prochain test, je vais essayer de trouver un équilibre dans la taille et essayer autre un élément qui remplace la poignée.

*TEST 2*

![](./images/esq3.jpg)

Deux autres tests sont effectués à partir de la première épreuve.

Dans ce cas, on remplace la section horizontale par de petites marques pour le bout des doigts.

D’autre part, En plus de remplacer la poignée, je cherche à trouver une taille confortable pour les mains et la géométrie de cône est changé en une géométrie carrée.

![](./images/prueba3.jpg)

Le professeur m’a recommandé d’imprimer tous les tests dans la même couleur, de cette façon je verrais mieux les erreurs. Donc j’imprime en blanc!

Je me rends compte que ce test n’a pas beaucoup de succès non plus. Mais je peux trouver des aspects positifs. Par exemple, la nouvelle forme carrée et la taille sont beaucoup plus confortables pour mes mains.

![](./images/roto.jpg)

L’impression 3D avait besoin d’un support à la base. À la fin de l’impression, j’ai été un peu brutale et en enlevant le matériau de support j’ai cassé l’objet. Je dois faire plus attention la prochaine fois.

*TEST 3*

![](./images/esq2.jpg)

Pour le test 3, on part également de l’épreuve 1.

En analysant vraiment la façon dont on prend une tasse, je me suis rendu compte que les doigts sont placés en diagonale et non à l’horizontale comme il est proposé dans le test 1.

Ce test 2 pose ces sections adaptées aux doigts du test 1 mais cette fois diagonales à l’objet.

![](./images/3d.jpg)

J’ai fait trois tests avec l’imprimante 3D qui ont échoué. J’ai alors supposé que le problème venait de la configuration que j’avais établie dans le logiciel de Prusa. Le problème était qu’il n’était pas bien fixé à la base. J’ai donc changé la configuration de la bordure, ajoutant plus de largeur. De cette façon la base est plus stable.

![](./images/prueba4.jpg)

Cette solution à l’objet fonctionne et s’adapte vraiment aux doigts. Mais d’un autre côté, il y a aussi des choses à améliorer, je dois faire un tour et réfléchir à ce qui se passe avec les autres doigts.

### MATIÈRE

1. Le matériau avec lequel je veux faire mes premiers tests est le **MÉTHACRYLATE.**

![](./images/metacrilato.jpg)

>Le méthacrylate est un matériau plastique transparent, très rigide et résistant aux agents atmosphériques.

Ce sont certaines de ses caractéristiques et propriétés qui peuvent nous être utiles pour construire notre tasse sont:

* 93 % de transparence
* Isolation thermique et acoustique
* C’est un substitut du verre mais très léger
* Haute résistance aux chocs
* Grande dureté
* Il est flexible mais ne peut se plier et changer sa forme qu’avec de la chaleur (à 80ºC)
* C'est facil à travailler (percer, poncer ou couper)

*Comment est-ce que je peux fabriquer l'objet avec les machines du FabLab?*

La première étape consiste à fabriquer un moule. Avec la pièce définie, une impression 3D est réalisée grâce aux machines FabLab.

Une fois le moule prêt, on procède à chauffer le méthacrylate pour pouvoir le plier avec la forme de notre moule.

*Quelles sont les techniques pour plier le méthacrylate?*

1. *TECHNIQUE DE THERMOFORMAGE*

![](./images/termoformadora.jpg)

La technique de thermoformage est un procédé qui sert à élaborer des produits à travers des machines de thermoformage. Il est basé sur la température et nous aide à produire des pièces principalement en plastique.

Il y a aussi la possibilité de fabriquer la machine de thermoformage dans nos maisons. Mais d'autre part, si je fabrique ma propre machine à thermoformage, je doute que les éléments que j’ai chez moi suffisent à changer la forme du méthacrylate.

![](./images/termofablab.jpg)

En faisant des recherches sur FabLab, j’ai réalisé que nous avions une machine de thermoformage. Cependant je trouve deux limites : d’une part la taille de la machine est petite pour mon objet ; et d’autre part elle n’a que la capacité de former du plastique de peu d’épaisseur.

### PRE JURY

Avant le jury final du 27 janvier, nous avons procédé à une dernière correction le 16 décembre. Nos projets y ont été exposés et présentés et nous avons reçu des commentaires des enseignants et des autres membres du jury.

Après avoir travaillé ces semaines sur la forme et sur la recherche d'une forme accueillante et adaptable à mes mains, je me suis rendu compte que je me suis détaché de la question de la chaleur. Quelle technique dois-je suivre pour obtenir l'effet que je recherche ? Réchauffer mes mains sans les brûler. Ce sont toutes mes conclusions et références pour continuer à travailler sur l'objet :

![](./images/JURY1.jpg)

1. Les tasses japonaises qui travaillent sur la question de l'épaisseur. Une tasse d'hiver.

2. Kitade Fujio, un céramiste.

![](./images/JURY2.jpg)

3. UNFOLD. Un studio de design qui travaille avec des machines d'impression 3D de céramique (fabrication de pièces en céramique aux formes uniques).

4. Johnson Tsnag, un sculpteur à la qualité unique : sculpter des pièces fantastiques qui déforment la réalité. Le style de l'artiste se caractérise par la création de visages modifiés. Ils ont des visages dans différents états d'esprit.

![](./images/correccion3.jpg)

5. Des filaments spéciaux qui possèdent ces deux qualités : isolant thermique et non-toxique.

6. Couche extérieure avec un filament flexible et de cette façon l'adapter à une tasse existante.

7. Modifiez le design en créant un objet allongé afin que le liquide reste en dessous et que l'on puisse l'attraper par le haut sans se brûler.

Cette correction m'a vraiment aidé à faire avancer le projet. Ce fut une journée intéressante, chaque membre du jury m'apportant de nouvelles idées et des points de vue différents.

### FORME

Le dernier test sur la question de la forme est de concevoir l’inverse de la conception finale. De cette façon la surface qui est en contact entre les doigts et la tasse est moins, et je peux travailler sur la question de la chaleur.

Il peut fonctionner, s’il est dessiné avec les mesures correctes, mais le design ci-dessus est plus esthétique et confortable.

### MATIÈRE

Finalement, la décision finale est de travailler avec la barbotine.

>> La barbotine est un mélange d’argile et d’eau de consistance barrose presque liquide.

Il y a de la barbotine à coller, mais dans mon cas j’utiliserai de la barbotine par coulée ou vidange. C’est une méthode pour la production de céramique. Son plus grand avantage est de répéter des centaines de fois une forme exacte. Particulièrement bon pour les formes qui ne sont pas faciles à faire sur le tour, comme c’est mon cas.

L’argile liquide est versée dans un moule en plâtre, le plâtre absorbe l’eau, dans les murs intérieurs du moule s’accumule la couche d’argile, quand il a l’épaisseur désirée le liquide restant est retiré en renversant le moule à l’envers. Laisser sécher pendant un certain temps jusqu’à ce que l’argile ait la consistance appropriée, de sorte que lors de l’ouverture, il soit facile de retirer la pièce. Là où les parties du moule se rejoignent, il reste une surface irrégulière à lisser. La pièce est ensuite séchée et devient ce qu’on appelle la céramique.

![](./images/barbotine.jpg)

## 5. ADAPTATION, DIFUSSION ÉDITION

![](./images/collecion.jpg)

La collection finale se composera de 5 tasses.

Les tasses représentées en blanc s’adaptent aux gestes naturels dans lesquels je prends une tasse. L’épaisseur s’adapte à la tasse de la même manière que la main s’adapte à la tasse, et joue en créant une esthétique intéressante.

Les tasses représentées en noir, par contre, sont "le négatif" des tasses blanches. L’avantage de celles-ci est que les doigts touchent une surface inférieure de la tasse, et de cette façon la question de la chaleur est résolue dans ces modèles.

![](./images/rhino.jpg)

A propos du logiciel, Rhinoceros 7, j’ai modélisé les corps. Ensuite, je les ai passés au logiciel Fusion 360, où j’ai vidé, coupé et assemblé ces corps.

### EXPLORATIONS

Quelles sont les manières possibles de faire le moule ?

![](./images/pruebass.jpg)


1. Le premier test que je fais est en utilisant la thermoformatrice de FabLab. Je l’ai déjà évaluée avant, donc nous savons comment cela fonctionne.

 Il n’a pas fonctionné car la forme de la tasse rend impossible de le sortir du moule. De toute façon, ce matériau (plastique) ne fonctionnerait pas avec la barbotine, car il a besoin de plâtre.

![](./images/pruebas.jpg)

2. Dans le cas précédent, j’ai réalisé que je devais diviser la tasse en deux parties pour pouvoir extraire la pièce finale du moule. C’est un test rapide pour faire plus tard le moule en plâtre.

3. J’ai pensé que ce serait une bonne idée d’imprimer le moule directement avec l’imprimante 3D, mais encore une fois, cela ne fonctionne pas avec la barbotine.

4. Essai en argile pour tester un mode de fabrication plus simple et moins long. Le résultat est très sale et n’obtient pas la forme désirée.

## FABRICATION. Commence mon désespoir.

![](./images/moldes.jpg)

On imprime les cinq dessins divisés en deux pour ensuite réaliser les moules en plâtre, adaptés à la barbotine. Jusqu’ici, tout va bien.

  ![](./images/encofrados.jpg)

Je prépare les coffrages pour mettre mes tasses et verser le plâtre. Beaucoup des répétitions que j’ai faites ont échoué et j’ai dû répéter le processus plusieurs fois. Ici, les choses commencent à mal tourner.

![](./images/yeso.jpg)

 Le plâtre prend plus de temps à sécher que prévu et je dois retarder l’étape suivante.

![](./images/barbotina.jpg)

Commencez les tests sur la barbotine. Pour celle-ci, je vais prendre un moule en plâtre pour l’extérieur et un moule en plastique pour obtenir l’épaisseur de la pièce. Évidemment, comme je le pensais, ça ne marche pas. Le plastique n’est pas compatible avec la barbotine, ils restent collés. Dans un deuxième essai, l’intérieur est un moule en plâtre, cette fois, j’espère que cela fonctionnera. Ça ne marche pas non plus, les murs ne sont pas couverts correctement.

![](./images/mas.jpg)

Un troisième essai avec la même technique : moule en plâtre à l’extérieur et à l’intérieur. Encore une fois, ce test n’a pas fonctionné. Le premier s’est cassé et le second n’a pas obtenu la bonne forme, et en plus on ne peut pas décoller du moule intérieur.

![](./images/hormigon.jpg)

À mon tour, je fais quelques tests avec les moules imprimés en 3D avec du béton. Je suis désespérée et je dois avoir un deuxième choix au cas où les choses tournent mal. Cette deuxième option a également échoué. Personnellement, je pense que l’erreur a été que l’épaisseur était très petite. En outre, certains moules imprimés en 3D ont été brisés.

![](./images/ultima.jpg)

J’ai failli jeter l’éponge, mais j’ai voulu faire un dernier test sur la barbotine. Cette fois, j’utiliserai la technique classique de la barbotine. Verser la barbotine dans le moule et attendre que les bords soient collés au moule (environ 20 minutes). De cette façon, vous ne pouvez pas contrôler l’épaisseur. J’ai dû me détacher de l’idée de l’épaisseur pour arriver à un résultat réel. J’attends maintenant cette dernière chance que le projet soit terminé..

![](./images/resultado.jpg)

Je n’ai finalement pas atteint mes objectifs. J’ai dû abandonner beaucoup d’idées en chemin et la qualité finale n’est pas celle que j’attendais.

Malgré cela, je suis heureux de toutes les compétences que j’ai acquises dans ce cours au niveau de Fablab, au niveau de la conception et au niveau des matériaux.

## LIENS UTILES

* <a href="https://a360.co/3rSqIxv" target="_blank" style="font-weight: bold; color: #1CAAD9;">TASSE 1</a>
* <a href="https://a360.co/3G3DCO8" target="_blank" style="font-weight: bold; color: #1CAAD9;">TASSE 2</a>
* <a href="https://a360.co/3KJTnNY" target="_blank" style="font-weight: bold; color: #1CAAD9;">TASSE 3</a>
* <a href="https://a360.co/32zRJx3" target="_blank" style="font-weight: bold; color: #1CAAD9;">TASSE 4</a>
* <a href="https://a360.co/33TGiRA" target="_blank" style="font-weight: bold; color: #1CAAD9;">TASSE 5</a>



